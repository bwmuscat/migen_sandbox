/* Machine-generated using Migen */
module top(
	output reg [3:0] m2s_addr,
	output reg m2s_we,
	output [7:0] m2s_data,
	input [7:0] s2m_data,
	input s2m_ack,
	input sys_clk,
	input sys_rst
);

reg [1:0] state = 2'd0;
reg [1:0] next_state;
reg we_next_value0;
reg we_next_value_ce0;
reg [3:0] addr_next_value1;
reg addr_next_value_ce1;

// synthesis translate_off
reg dummy_s;
initial dummy_s <= 1'd0;
// synthesis translate_on

assign m2s_data = ((m2s_addr * 2'd3) + 2'd2);

// synthesis translate_off
reg dummy_d;
// synthesis translate_on
always @(*) begin
	next_state <= 2'd0;
	we_next_value0 <= 1'd0;
	we_next_value_ce0 <= 1'd0;
	addr_next_value1 <= 4'd0;
	addr_next_value_ce1 <= 1'd0;
	next_state <= state;
	case (state)
		1'd1: begin
			addr_next_value1 <= (m2s_addr + 1'd1);
			addr_next_value_ce1 <= 1'd1;
			if ((m2s_addr == 3'd7)) begin
				we_next_value0 <= 1'd0;
				we_next_value_ce0 <= 1'd1;
				next_state <= 2'd2;
			end
		end
		2'd2: begin
			addr_next_value1 <= (m2s_addr + 1'd1);
			addr_next_value_ce1 <= 1'd1;
			if ((m2s_addr == 3'd7)) begin
				next_state <= 2'd3;
			end
		end
		2'd3: begin
			next_state <= 2'd3;
		end
		default: begin
			next_state <= 1'd1;
			we_next_value0 <= 1'd1;
			we_next_value_ce0 <= 1'd1;
		end
	endcase
// synthesis translate_off
	dummy_d <= dummy_s;
// synthesis translate_on
end

always @(posedge sys_clk) begin
	state <= next_state;
	if (we_next_value_ce0) begin
		m2s_we <= we_next_value0;
	end
	if (addr_next_value_ce1) begin
		m2s_addr <= addr_next_value1;
	end
	if (sys_rst) begin
		m2s_addr <= 4'd0;
		m2s_we <= 1'd0;
		state <= 2'd0;
	end
end

endmodule
