/* Machine-generated using Migen */
module top(
	input sys_clk,
	input sys_rst
);

wire [5:0] m2s_addr;
wire [7:0] m2s_data;
wire s2m_ack;
wire s2m_error;
wire m2s_sel;
wire m2s_we;
wire [7:0] s2m_data;


Module_Mem Module_Mem(
	.m2s_addr(m2s_addr),
	.m2s_data(m2s_data),
	.m2s_sel(m2s_sel),
	.m2s_we(m2s_we),
	.sys_clk(sys_clk),
	.sys_rst(sys_rst),
	.s2m_ack(s2m_ack),
	.s2m_data(s2m_data),
	.s2m_error(s2m_error)
);

Module_Master Module_Master(
	.s2m_ack(s2m_ack),
	.s2m_data(s2m_data),
	.s2m_error(s2m_error),
	.sys_clk(sys_clk),
	.sys_rst(sys_rst),
	.m2s_addr(m2s_addr),
	.m2s_data(m2s_data),
	.m2s_sel(m2s_sel),
	.m2s_we(m2s_we)
);

endmodule
