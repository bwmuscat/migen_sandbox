/* Machine-generated using Migen */
module top(
	output [3:0] adr,
	output [31:0] dat_w,
	output reg s2m_ack,
	output s2m_error,
	input m2s_sel,
	input m2s_we,
	output reg [31:0] s2m_data,
	input sys_clk,
	input sys_rst
);

wire [31:0] dat_r;
wire we;
reg [31:0] m2s_data = 32'd0;
reg [31:0] m2s_addr = 32'd0;

// synthesis translate_off
reg dummy_s;
initial dummy_s <= 1'd0;
// synthesis translate_on

assign s2m_error = (adr > 4'd10);
assign we = (m2s_sel & m2s_we);
assign adr = m2s_addr;
assign dat_w = m2s_data;

// synthesis translate_off
reg dummy_d;
// synthesis translate_on
always @(*) begin
	s2m_data <= 32'd0;
	if (m2s_sel) begin
		s2m_data <= dat_r;
	end else begin
		s2m_data <= 1'd0;
	end
// synthesis translate_off
	dummy_d <= dummy_s;
// synthesis translate_on
end

always @(posedge sys_clk) begin
	s2m_ack <= m2s_sel;
	if (sys_rst) begin
		s2m_ack <= 1'd0;
	end
end

reg [31:0] mem[0:9];
reg [3:0] memadr;
always @(posedge sys_clk) begin
	if (we)
		mem[adr] <= dat_w;
	memadr <= adr;
end

assign dat_r = mem[memadr];

endmodule
