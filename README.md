## How to use it:
1. Install Anaconda
1. Install migen
1. Run `mem_controller.ipynb` in Jupyter Notebook

## Files:
* `mem_controller.ipynb` - **Main file, read it first**
* `mem_module.v` - RAM wrapper (RTL)
* `mst_module.v` - Read/Write transaction generator (RTL)
* `top_tb.v` - RAM wrapper + Generator, ready for synth
* `mem_test.vcd` - Waveform of memory testing
* `master_test.vcd` - - Waveform of transaction generator testing
* `master_test_rtl.png` - Drawings for IPython
* `master_test_sim.PNG` - Drawings for IPython
